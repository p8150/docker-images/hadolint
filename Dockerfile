FROM alpine:3.16
ENV version="2.10.0"
COPY build/download_hadolint.sh /tmp/
COPY build/.hadolint.yaml /root/.hadolint.yaml
RUN /tmp/download_hadolint.sh
LABEL version=${version}
