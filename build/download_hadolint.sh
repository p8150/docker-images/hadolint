#!/bin/sh
version=${version:-2.10.0}
case "$(uname -m)" in
  "aarch64")
    export ARCH=arm64 ;;
  "arm64")
    export ARCH=arm64 ;;
  *)
    export ARCH=x86_64 ;;
esac
echo "https://github.com/hadolint/hadolint/releases/download/v${version}/hadolint-Linux-${ARCH}"
wget -q -O /usr/bin/hadolint "https://github.com/hadolint/hadolint/releases/download/v${version}/hadolint-Linux-${ARCH}" && \
chmod +x /usr/bin/hadolint